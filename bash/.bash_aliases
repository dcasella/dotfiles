#!/usr/bin/env bash

alias l='ls'
alias ll='ls -hl'
alias la='ls -A'
alias lla='ls -hAl'
alias df='df -Th -x tmpfs -x shm -x overlay'
alias ...='../..'
alias ....='../../..'
alias wget="wget --hsts-file='\${XDG_DATA_HOME}/wget-hsts'"
alias yarn="yarn --use-yarnrc '\${XDG_CONFIG_HOME}/yarn/config'"

cd() {
	builtin cd "$@" || exit "${?}"
	ls
}
