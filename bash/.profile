#!/usr/bin/env bash

umask 0022

# if running bash
if [ -n "$BASH_VERSION" ]; then
	# include .bashrc if it exists
	if [ -f "$HOME/.bashrc" ]; then
		. "$HOME/.bashrc"
	fi
fi

# Handle gpg-agent and ssh-agent emulation
if command -v gpgconf &> /dev/null; then
	unset SSH_AGENT_PID

	if [ "${gnupg_SSH_AUTH_SOCK_by:-0}" -ne $$ ]; then
		SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"
		export SSH_AUTH_SOCK
	fi

	GPG_TTY=$(tty)
	export GPG_TTY

	gpg-connect-agent updatestartuptty /bye &> /dev/null
fi
