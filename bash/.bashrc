#!/usr/bin/env bash

if [ -f ~/.bash_env ]; then
	. ~/.bash_env
fi

# exit if not running interactively
case $- in
*i*) ;;
*) return;;
esac

# cleanup WSL path
# bash prompt in WSL gets very slow with a lot of useless elements in PATH...
if grep -q microsoft /proc/version; then
	IFS=: read -ra path_components_orig <<< "${PATH}"
	path_components=()

	for path_component in "${path_components_orig[@]}"; do
		case "${path_component}" in
		# Windows paths
		*'VS Code/bin')
			# keep VS Code from Windows
			path_components+=("${path_component}")
			;;
		/c/*|/mnt/c/*)
			# remove other Windows paths
			;;
		/*)
			# keep all Linux paths
			path_components+=("${path_component}")
			;;
		*)
			# remove everything else (is there anything left, really?)
			;;
		esac
	done

	IFS=: PATH="${path_components[*]}"
	export PATH

	unset path_component path_components path_components_orig
fi

# history configuration
shopt -s histappend
HISTCONTROL=ignoreboth # no duplicate lines or lines starting with space
HISTSIZE=10000
HISTFILESIZE=20000

# general options
shopt -s checkwinsize # update the values of LINES and COLUMNS after each command
shopt -s globstar # enable **

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
	debian_chroot=$(cat /etc/debian_chroot)
fi

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
	if [ -r ~/.dircolors ]; then
		eval "$(dircolors -b ~/.dircolors)"
	else
		eval "$(dircolors -b)"
	fi
	alias ls='ls --color=auto'
	alias grep='grep --color=auto'
	alias fgrep='fgrep --color=auto'
	alias egrep='egrep --color=auto'
fi

if [ -f ~/.bash_aliases ]; then
	. ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
	. /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
	. /etc/bash_completion
  fi
fi

# ble.sh - interactive
source ~/.local/share/blesh/ble.sh --noattach

# prompt setup
if [ ${UID} -ne 0 ]; then
	PR_USER='\e[0;32m\u\e[m'
else
	PR_USER='\e[0;31m\u\e[m'
fi
if [ -n "${SSH_CLIENT}" ] || [ -n "${SSH2_CLIENT}" ]; then
	PR_HOST='@\e[0;32m\h\e[m'
else
	PR_HOST=''
fi
PR_PATH='\e[0;36m\w\e[m'

export PS1="┌ ${PR_USER}${PR_HOST} ${PR_PATH} \q{contrib/git-branch}\n└ "

# ble.sh - attach
[[ ! ${BLE_VERSION-} ]] || ble-attach
