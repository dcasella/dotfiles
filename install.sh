#!/usr/bin/env bash

set -o errexit  # exit script when a command fails
set -o pipefail # non-zero exit codes propagate to the end of a pipeline

usage() {
	tee <<-EOD
	USAGE:
	    ${0##*/} [FLAG...] [OPTION...]

	FLAGS:
	    -h                Display this message

	    All flags exit the program
	    Flags are parsed in the displayed order

	OPTIONS:
	    -f                Add --adopt flag to stow command

	PARAMETERS:
	    TARGET            "client" or "server"
	EOD
}

stow_opts=

while getopts ':hf' opt; do
	case "${opt}" in
	h)
		usage
		exit
		;;
	f)
		stow_opts="${stow_opts} --adopt"
		;;
	\?)
		echo "Option -${OPTARG} is invalid"
		exit 1
		;;
	:)
		echo "Option -${OPTARG} requires an argument"
		exit 1
		;;
	esac
done
shift $((OPTIND-1))

if [ $# -ne 1 ]; then
	usage
	exit 1
fi

mode="${1}"

# validation
required_commands=(git awk make)
for reqcmd in "${required_commands[@]}"; do
	if ! command -v "${reqcmd}" &> /dev/null; then
		echo "[error] ${reqcmd} is not installed" >&2
	fi
done

# install ble.sh
if [ ! -d "${HOME}/.local/share/blesh" ]; then
	cd /tmp
	rm -rf ble.sh
	git clone --recursive --depth 1 --shallow-submodules https://github.com/akinomyoga/ble.sh.git
	make -C ble.sh install PREFIX=~/.local
	rm -rf ble.sh
	cd - > /dev/null
fi

case "${mode}" in
client)
	stow_list=(kde gnupg npm tmux bash)
	;;
server)
	stow_list=(tmux bash)
	;;
esac

# execute stow
# shellcheck disable=SC2086
stow -R -t "${HOME}" ${stow_opts} "${stow_list[@]}"
echo '[info] done'

# update current shell
echo '[info] to update the current shell:'
echo '. ~/.bashrc'
